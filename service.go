package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"io/ioutil"
	"net/http"
)

type WEIGHTSTRATEGY string

const (
	BOTH               = WEIGHTSTRATEGY("BOTH")
	ONLY_RESPONSE_TIME = WEIGHTSTRATEGY("ONLY_RESPONSE_TIME ")
	ONLY_AVAILABILITY  = WEIGHTSTRATEGY("ONLY_AVAILABILITY")
)

func SendRepairMessage(repairTask RepairTask) {
	body := JsonToString(StructToJson(repairTask))
	err := ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	if err != nil {
		panic(err)
	}
}

func GetTMax(workflowAbstract WorkflowAbstract) float64 {
	var concreteMap ConcreteMap
	resp, err := http.Post(pathToOptimalOchestratorTMax, "application/json; charset=UTF-8",
		bytes.NewBuffer(StructToJson(workflowAbstract)))
	if err != nil {
		// handle error
		//panic(err)
		fmt.Println("Errore nell'eseguire GetTMax() di" + workflowAbstract.Uid)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//panic(err)
		fmt.Println("Errore nel JsonParsing di GetTMax() di" + workflowAbstract.Uid)
	}
	if err = json.Unmarshal(body, &concreteMap); err != nil {
		fmt.Println("Errore nell' Unmarshal di GetTMax() di" + workflowAbstract.Uid)
	}
	return concreteMap.R
}

func GetTMin(workflowAbstract WorkflowAbstract) float64 {
	var concreteMap ConcreteMap
	resp, err := http.Post(pathToOptimalOchestratorTMin, "application/json; charset=UTF-8",
		bytes.NewBuffer(StructToJson(workflowAbstract)))
	if err != nil {
		// handle error
		//panic(err)
		fmt.Println("Errore nell'eseguire GetTMin() di" + workflowAbstract.Uid)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//panic(err)
		fmt.Println("Errore nel JsonParsing di GetTMin() di" + workflowAbstract.Uid)
	}
	if err = json.Unmarshal(body, &concreteMap); err != nil {
		fmt.Println("Errore nell' Unmarshal di GetTMin() di" + workflowAbstract.Uid)
	}
	return concreteMap.R
}
func GetAMax(workflowAbstract WorkflowAbstract) float64 {
	var concreteMap ConcreteMap
	resp, err := http.Post(pathToOptimalOchestratorAMax, "application/json; charset=UTF-8",
		bytes.NewBuffer(StructToJson(workflowAbstract)))
	if err != nil {
		// handle error
		//panic(err)
		fmt.Println("Errore nell'eseguire GetAMax() di" + workflowAbstract.Uid)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//panic(err)
		fmt.Println("Errore nel JsonParsing di GetAMax() di" + workflowAbstract.Uid)
	}
	if err = json.Unmarshal(body, &concreteMap); err != nil {
		fmt.Println("Errore nell' Unmarshal di GetAMax() di" + workflowAbstract.Uid)
	}
	return concreteMap.LogA
}

func GetAMin(workflowAbstract WorkflowAbstract) float64 {
	var concreteMap ConcreteMap
	resp, err := http.Post(pathToOptimalOchestratorAMin, "application/json; charset=UTF-8",
		bytes.NewBuffer(StructToJson(workflowAbstract)))
	if err != nil {
		// handle error
		//panic(err)
		fmt.Println("Errore nell'eseguire GetAMin() di" + workflowAbstract.Uid)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//panic(err)
		fmt.Println("Errore nel JsonParsing di GetAMin() di" + workflowAbstract.Uid)
	}
	if err = json.Unmarshal(body, &concreteMap); err != nil {
		fmt.Println("Errore nell' Unmarshal di GetAMin() di" + workflowAbstract.Uid)
	}
	return concreteMap.LogA
}

func Analize(workflowAbstract WorkflowAbstract, concreteMap ConcreteMap) bool {
	weightstrategy := WeightStrategy()
	if weightstrategy == BOTH {

		//tmax := GetTMax(workflowAbstract)
		tmin := GetTMin(workflowAbstract)
		amax := GetAMax(workflowAbstract)
		//amin := GetAMin(workflowAbstract)

		// ????????????????????????????????????????????????
		// C'è una soluzione migliore!!!
		if tmin < concreteMap.R && amax > concreteMap.LogA {
			fmt.Println("C'è una soluzione migliore!")
			return true
		} else if tmin > concreteMap.R && amax < concreteMap.LogA {
			// potrei mettere una soglia
			fmt.Println("La soluzione migliore di prima non va più bene!")
			return true
		} else {
			return false
		}

		// valutare secondo opportuna tecnica il repair
		//
	} else if weightstrategy == ONLY_RESPONSE_TIME {
		//tmax := GetTMax(workflowAbstract)
		tmin := GetTMin(workflowAbstract)

		if concreteMap.TMin > tmin {
			fmt.Println("C'è una soluzione migliore!")
			return true
		} else if concreteMap.TMin < tmin {
			// potrei mettere una soglia
			fmt.Println("La soluzione migliore di prima non va più bene!")
			return true
		} else {
			//in teoria dovrebbe essere quella di prima
			return false
		}

	} else {
		amax := GetAMax(workflowAbstract)
		//amin := GetAMin(workflowAbstract)
		if concreteMap.LogA < amax {
			fmt.Println("C' è una soluzione migliore!")
			return true
		} else if amax < concreteMap.LogA {
			// potrei mettere una soglia
			fmt.Println("La soluzione migliore di prima non va più bene!")
			return true
		} else {
			//in teoria dovrebbe essere quella di prima
			return false
		}
	}

}

func WeightStrategy() WEIGHTSTRATEGY {
	var weight WeightStrategyParams
	resp, err := http.Get(pathToOptimalOchestratorWeight)
	if err != nil {
		// handle error
		//panic(err)
		fmt.Println("Errore nella Get di WeightStrategy()")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//panic(err)
		fmt.Println("Errore nel ReadAll() di WeightStrategy()")
	}
	if err = json.Unmarshal(body, &weight); err != nil {
		fmt.Println("Errore nell'Unmarshal di WeightStrategy()")
	}
	if weight.WA == "1.0" && weight.WR == "0.0" {
		return ONLY_AVAILABILITY
	} else if weight.WA == "0.0" && weight.WR == "1.0" {
		return ONLY_RESPONSE_TIME
	} else {
		return BOTH
	}
}
