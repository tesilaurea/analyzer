package main

import (
	"github.com/streadway/amqp"
)

var pathRabbitMQ string
var pathMongoGetWorkflowList string
var pathMongoGetConcreteMap string
var pathToOptimalOchestratorTMax string
var pathToOptimalOchestratorTMin string
var pathToOptimalOchestratorAMax string
var pathToOptimalOchestratorAMin string
var pathToOptimalOchestratorWeight string
var queue string

var conn *amqp.Connection
var ch *amqp.Channel
var q amqp.Queue
