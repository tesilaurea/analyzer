package main

import (
	_ "bytes"
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

/* Qui inserisco la classe di qui devo fare il Json parser
 */

func main() {
	pathRabbitMQ = "amqp://guest:guest@" + os.Getenv("RABBITMQ_HOST") + ":5672/"
	pathMongoGetWorkflowList = "http://localhost:8081/workflows/get/deployed"
	pathMongoGetConcreteMap = "http://localhost:8081/join/get/"
	pathToOptimalOchestratorTMax = "http://localhost:8080/optimal/submit/workflow/v2/tmax"
	pathToOptimalOchestratorTMin = "http://localhost:8080/optimal/submit/workflow/v2/tmin"
	pathToOptimalOchestratorAMax = "http://localhost:8080/optimal/submit/workflow/v2/amax"
	pathToOptimalOchestratorAMin = "http://localhost:8080/optimal/submit/workflow/v2/amin"
	pathToOptimalOchestratorWeight = "http://localhost:8080/optimal/get/weight"
	queue = "repair"
	var err error
	conn, err = amqp.Dial(pathRabbitMQ)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err = conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	//??
	q, err = ch.QueueDeclare(
		queue, // name
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	forever := make(chan bool)

	go func() {
		for {
			var workflowAbstractList []WorkflowAbstract
			//---------------------------------------------
			resp, err := http.Get(pathMongoGetWorkflowList)

			if err == nil {
				// handle error

				body, err := ioutil.ReadAll(resp.Body)

				if err := json.Unmarshal(body, &workflowAbstractList); err != nil {
					fmt.Println(err)
				}
				fmt.Println(workflowAbstractList)
				//---------------------------------------------
				for _, workflow := range workflowAbstractList {
					var concreteMap ConcreteMap
					resp, err = http.Get(pathMongoGetConcreteMap + workflow.Uid)
					if err != nil {
						// handle error
						//panic(err)
						fmt.Println("Errore nel ricevere il concreteMap di " + workflow.Uid)
					}

					isRepair := Analize(workflow, concreteMap)
					if isRepair {
						repairTask := makeRepairTask("", "", workflow.Uid, "", "repair", "", "", "")
						SendRepairMessage(repairTask)
					} else {
						continue
					}
				}
			} else {
				fmt.Println("Nessun elemento")
			}
			time.Sleep(time.Second * 30)
		}

	}()

	log.Printf("Inizio il ciclo MAPE per la fase di Repair")
	<-forever
}
